<?php
/**
 * The template for displaying Search Results pages.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

get_header(); ?>
<header id="h-blog-page" role="banner">
	<div class="row">
		<div class="large-1 small-4 columns">
			<a href="/base">
				<img src="<?php echo get_template_directory_uri(); ?>/img/logo-footer.png" alt="logo" />
			</a>
		</div>
		<div class="large-11  columns">
			<nav id="site-navigation" class="main-navigation" role="navigation">
				<h3 class="menu-toggle"><?php _e( 'Menu', 'twentytwelve' ); ?></h3>
				<a class="assistive-text" href="#content" title="<?php esc_attr_e( 'Skip to content', 'twentytwelve' ); ?>"><?php _e( 'Skip to content', 'twentytwelve' ); ?></a>
				<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'nav-menu' ) ); ?>
			</nav>
		</div>
	</div>
</header>
<div id="main-content">
	<div class="row">
<!--MAIN-->
		<div class="large-8 columns small-12">
			<?php if ( have_posts() ) : ?>
			
				<header class="page-header">
					<h1 class="page-title"><?php printf( __( 'Search Results for: %s', 'twentytwelve' ), '<span>' . get_search_query() . '</span>' ); ?></h1>
				</header>
	
				<?php twentytwelve_content_nav( 'nav-above' ); ?>
	
				<?php /* Start the Loop */ ?>
				<?php while ( have_posts() ) : the_post(); ?>
					<?php get_template_part( 'content', get_post_format() ); ?>
				<?php endwhile; ?>
	
				<?php twentytwelve_content_nav( 'nav-below' ); ?>
	
			<?php else : ?>
	
				<article id="post-0" class="post no-results not-found">
					<header class="entry-header">
						<h1 class="entry-title"><?php _e( 'Nothing Found', 'twentytwelve' ); ?></h1>
					</header>
	
					<div class="entry-content">
						<p><?php _e( 'Sorry, but nothing matched your search criteria. Please try again with some different keywords.', 'twentytwelve' ); ?></p>
						<?php get_search_form(); ?>
					</div><!-- .entry-content -->
				</article><!-- #post-0 -->
			
			<?php endif; ?>
		</div>
<!--MAIN-->
<!--RIGHT-->
		<div class="large-4 columns">
			<!--SEARCH-->
					<div id="form-search">
						<form id="searchform" method="get" action="<?php echo $_SERVER['PHP_SELF']; ?>">
						<div class="row">
							<div class="large-7 small-8 mediun-8 columns"><input type="text" name="s" id="s" class="large-12"/></div>
							<div class="large-5 small-4 mediun-8 columns">
								<input class="button large-12" type="submit" value="<?php _e('Search'); ?>" />
							</div>
						</div>
						</form>
					</div>
			<!--SEARCH-->
			<!--<?php get_sidebar(); ?>-->
<!--RELATIVE POST-->
		<div class="row" id="relative_post">
			<?php
			$tags = wp_get_post_tags($post->ID);
			if ($tags) {
			  $first_tag = $tags[0]->term_id;
			  $args=array(
			    'tag__in' => array($first_tag),
			    'post__not_in' => array($post->ID),
			    'showposts'=>3,
			    'caller_get_posts'=>1
			   );
			  $my_query = new WP_Query($args);
			  if( $my_query->have_posts() ) {
			    while ($my_query->have_posts()) : $my_query->the_post(); ?>
			    <div class="large-6 small-6 columns">
			      <a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_post_thumbnail(); ?></a>
			      <!--<p><?php the_excerpt(); ?></p>-->
			</div>
			      <?php
			    endwhile;
			  }
			wp_reset_query();}?>
		</div>
<!--RELATIVE POST-->
<!--RECENT POST-->
			<ul class="mywidget">
			<p class="subheader">Recientes</p>
			<?php
				$args = array( 'numberposts' => '6', 'tax_query' => array(
						array(
							'taxonomy' => 'post_format',
							'field' => 'slug',
							'terms' => 'post-format-aside',
							'operator' => 'NOT IN'
						), 
						array(
							'taxonomy' => 'post_format',
							'field' => 'slug',
							'terms' => 'post-format-image',
			 				'operator' => 'NOT IN'
						)
				) );
				$recent_posts = wp_get_recent_posts( $args );
				foreach( $recent_posts as $recent ){
					echo '<a href="' . get_permalink($recent["ID"]) . '" title="Look '.esc_attr($recent["post_title"]).'" >'.'<li>' . $recent["post_title"] . '</li></a> ';
				}
			?>
			</ul>
<!--RECENT POST-->
<!--FOLLOW-->
		<div class="follow">
			<div class="row">
				<div class="large-3 small-3 columns">
					<a target="_blank" href="http://www.behance.net/cesarmedrano">
						<img src="<?php echo get_template_directory_uri(); ?>/img/behance.png" alt="Cesar Medrano" />
					</a>
				</div>
				<div class="large-3 small-3 columns">
					<a target="_blank" href="https://www.facebook.com/pages/Cesar-Medrano/240082449394123">
						<img src="<?php echo get_template_directory_uri(); ?>/img/facebook.png" alt="facebook.com/cesaremedrano" />
					</a>
				</div>
				<div class="large-3 small-3 columns">
					<a target="_blank" href="https://twitter.com/cesar_medrano">
						<img src="<?php echo get_template_directory_uri(); ?>/img/twitter.png" alt="@cesar_medrano" />
					</a>
				</div>
				<div class="large-3 small-3 columns">
					<a target="_blank" href="http://www.flickr.com/photos/cesarmedrano/">
						<img src="<?php echo get_template_directory_uri(); ?>/img/flickr.png" alt="Cesar Medrano" />
					</a>
				</div>
			</div>
		</div>
<!--FOLLOW-->
		</div>
<!--RIGHT-->
	</div>
</div>
<?php get_footer(); ?>