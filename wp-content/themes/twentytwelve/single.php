<?php
/**
 * @package WordPress
 * @subpackage Base
 * @since base 1.0
 */

get_header(); ?>
<header id="h-blog-page" role="banner">
	<div class="row">
		<div class="large-1 small-4 columns">
			<a href="/">
				<img src="<?php echo get_template_directory_uri(); ?>/img/logo-footer.png" alt="logo" />
			</a>
		</div>
		<div class="large-11 small-8  columns">
			<?php
			
			$defaults = array(
				'theme_location'  => '',
				'menu'            => '',
				'container'       => 'div',
				'container_class' => '',
				'container_id'    => '',
				'menu_class'      => 'sub-nav',
				'menu_id'         => '',
				'echo'            => true,
				'fallback_cb'     => 'wp_page_menu',
				'before'          => '',
				'after'           => '',
				'link_before'     => '',
				'link_after'      => '',
				'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
				'depth'           => 0,
				'walker'          => ''
			);
			
			wp_nav_menu( $defaults );
			
			?>
			
		</div>
	</div>
</header>
<div id="main-content">
	<div class="row">
<!--MAIN-->
		<div class="large-8 columns small-12">
			<?php while ( have_posts() ) : the_post(); ?>
			<h1 class="gabriela subheader">
				<a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( sprintf( __( 'Permalink to %s', 'twentytwelve' ), the_title_attribute( 'echo=0' ) ) ); ?>" rel="bookmark"><?php the_title(); ?></a>
			</h1>
			<time datetime="<?php the_time('Y-m-d') ?>"><?php the_time('l j F, Y') ?></time>
				<div class="entry-content">
					<?php the_content(); ?>
					<?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'twentyten' ), 'after' => '</div>' ) ); ?>
				</div>
				<nav class="nav-single">
					<h3 class="assistive-text"><?php _e( 'Post navigation', 'twentytwelve' ); ?></h3>
					<span class="nav-previous"><?php previous_post_link( '%link', '%title' ); ?></span>
					<span class="nav-next"><?php next_post_link( '%link', '%title'); ?></span>
				</nav>
			<?php endwhile; // end of the loop. ?>
		</div>
<!--MAIN-->
<!--RIGHT-->
		<div class="large-4 columns">
			<!--SEARCH-->
					<div id="form-search">
						<form class="searchform radius" method="get" action="<?php echo $_SERVER['PHP_SELF']; ?>">
						<div class="row">
							<div class="large-7 small-8 mediun-8 columns"><input  type="text" name="s" id="s" class="large-12"/></div>
							<div class="large-5 small-4 mediun-8 columns">
								<input class="button large-12" type="submit" value="<?php _e('Search'); ?>" />
							</div>
						</div>
						</form>
					</div>
			<!--SEARCH-->
			<!--<?php get_sidebar(); ?>-->
<!--RELATIVE POST-->
		<div class="row" id="relative_post">
			<?php
			$tags = wp_get_post_tags($post->ID);
			if ($tags) {
			  $first_tag = $tags[0]->term_id;
			  $args=array(
			    'tag__in' => array($first_tag),
			    'post__not_in' => array($post->ID),
			    'showposts'=>3,
			    'caller_get_posts'=>1
			   );
			  $my_query = new WP_Query($args);
			  if( $my_query->have_posts() ) {
			    while ($my_query->have_posts()) : $my_query->the_post(); ?>
			    <div class="large-6 small-6 columns">
			      <a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>"><?php the_post_thumbnail(); ?></a>
			      <!--<p><?php the_excerpt(); ?></p>-->
			</div>
			      <?php
			    endwhile;
			  }
			wp_reset_query();}?>
		</div>
<!--RELATIVE POST-->
<!--RECENT POST-->
		<article class="widget-content">
			<h4 class="widget-tittle">Recientes</h4>
			<ul class="mywidget">
			<?php
				$args = array( 'numberposts' => '5', 'tax_query' => array(
						array(
							'taxonomy' => 'post_format',
							'field' => 'slug',
							'terms' => 'post-format-aside',
							'operator' => 'NOT IN'
						), 
						array(
							'taxonomy' => 'post_format',
							'field' => 'slug',
							'terms' => 'post-format-image',
			 				'operator' => 'NOT IN'
						)
				) );
				$recent_posts = wp_get_recent_posts( $args );
				foreach( $recent_posts as $recent ){
					echo '<li><a href="' . get_permalink($recent["ID"]) . '" title="Look '.esc_attr($recent["post_title"]).'" >' .   $recent["post_title"].'</a> </li> ';
				}
			?>
			</ul>
		</article>
<!--RECENT POST-->
<!--FOLLOW-->
		<div class="follow">
			<div class="row">
				<div class="large-3 small-3 columns">
					<a target="_blank" href="http://www.behance.net/cesarmedrano">
						<img src="<?php echo get_template_directory_uri(); ?>/img/behance.png" alt="Cesar Medrano" />
					</a>
				</div>
				<div class="large-3 small-3 columns">
					<a target="_blank" href="https://www.facebook.com/pages/Cesar-Medrano/240082449394123">
						<img src="<?php echo get_template_directory_uri(); ?>/img/facebook.png" alt="facebook.com/cesaremedrano" />
					</a>
				</div>
				<div class="large-3 small-3 columns">
					<a target="_blank" href="https://twitter.com/cesar_medrano">
						<img src="<?php echo get_template_directory_uri(); ?>/img/twitter.png" alt="@cesar_medrano" />
					</a>
				</div>
				<div class="large-3 small-3 columns">
					<a target="_blank" href="http://www.flickr.com/photos/cesarmedrano/">
						<img src="<?php echo get_template_directory_uri(); ?>/img/flickr.png" alt="Cesar Medrano" />
					</a>
				</div>
			</div>
		</div>
<!--FOLLOW-->
		</div>
<!--RIGHT-->
	</div>
</div>
<?php get_footer(); ?>