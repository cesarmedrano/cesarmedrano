<?php
/**
 * Template Name: Blog template
 *
 * Description: Twenty Twelve loves the no-sidebar look as much as
 * you do. Use this page template to remove the sidebar from any page.
 *
 * Tip: to remove the sidebar from all posts and pages simply remove
 * any active widgets from the Main Sidebar area, and the sidebar will
 * disappear everywhere.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */
get_header(); ?>
<header id="h-blog-page" role="banner">
	<div class="row">
		<div class="large-1 small-4 columns">
			<a href="/base">
				<img src="<?php echo get_template_directory_uri(); ?>/img/logo-footer.png" alt="logo" />
			</a>
		</div>
		<div class="large-11  columns">
			<nav id="site-navigation" class="main-navigation" role="navigation">
				<h3 class="menu-toggle"><?php _e( 'Menu', 'twentytwelve' ); ?></h3>
				<a class="assistive-text" href="#content" title="<?php esc_attr_e( 'Skip to content', 'twentytwelve' ); ?>"><?php _e( 'Skip to content', 'twentytwelve' ); ?></a>
				<?php wp_nav_menu( array( 'theme_location' => 'primary', 'menu_class' => 'nav-menu' ) ); ?>
			</nav>
		</div>
	</div>
</header>
<div id="main-content">
	<div class="row">
		<div class="large-8 columns small-12">
			<h2 id="post-<?php the_ID(); ?>">
				<a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title(); ?>">
				<?php the_title(); ?></a>
			</h2><!--Titulo pagina-->
			<?php
			$numposts = $wpdb->get_var("SELECT COUNT(*) FROM $wpdb->posts WHERE post_status = 'publish'");
			if (0 < $numposts) $numposts = number_format($numposts);?>
			 <ul id="archive-list" class="row">
			 <?php
			 $myposts = get_posts('numberposts=-1&');
			 foreach($myposts as $post) : ?>
			 <li class="large-12 small-6 columns item-article">
			 	<div class="large-4 columns">
			 		<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
			 	</div>
			 	<div class="large-8 columns">
			 		<a href="<?php the_permalink(); ?>">
			 			<h5><?php if (strlen($post->post_title) > 28) { 
			 				echo substr(the_title($before = '', $after = '', FALSE), 0,28) . '...'; 
			 			} 
			 				else { the_title(); } ?>
			 			</h5>
			 		</a>
			 		<small><?php the_time('l j F , Y') ?></small> 
			 	</div>
			 </li>
			 <?php endforeach; ?>
			 </ul>
		</div>
		<div class="large-4 columns"><?php get_sidebar(); ?></div>
	</div>
</div>
<div id="sub-footer">
	<div class="row">
		<div class="large-4 columns">
			<?php if (function_exists('get_least_viewed_tag')): ?>
			    <ul>
			        <?php get_least_viewed_tag(); ?>
			    </ul>
			<?php endif; ?>
		</div>
	</div>
</div>
	

<?php get_footer(); ?>