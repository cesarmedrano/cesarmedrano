<?php
/**
 * Template Name: Front Page Template
 *
 * Description: A page template that provides a key component of WordPress as a CMS
 * by meeting the need for a carefully crafted introductory page. The front page template
 * in Twenty Twelve consists of a page content area for adding text, images, video --
 * anything you'd like -- followed by front-page-only widgets in one or two columns.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

get_header();?>
<!--get_header(); -->
<header id="h-front-page" role="banner">
	<div class="row">
		<div class="large-1 small-4 columns">
			<a href="/">
				<img src="<?php echo get_template_directory_uri(); ?>/img/logo-main.png" alt="logo" />
			</a>
		</div>
		<div class="large-11 small-8 columns">
			<?php
			
			$defaults = array(
				'theme_location'  => '',
				'menu'            => '',
				'container'       => 'div',
				'container_class' => 'right',
				'container_id'    => 'main-menu-top',
				'menu_class'      => 'sub-nav',
				'menu_id'         => '',
				'echo'            => true,
				'fallback_cb'     => 'wp_page_menu',
				'before'          => '',
				'after'           => '',
				'link_before'     => '',
				'link_after'      => '',
				'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
				'depth'           => 0,
				'walker'          => ''
			);
			
			wp_nav_menu( $defaults );
			
			?>
		</div>
	</div>
	<div class="row">
		<div class="large-12 columns">
			<div id="cloud-down" class="row">
				<div class="large-8 columns">
					<div id="globo-content">
						<img id="globo" src="<?php echo get_template_directory_uri(); ?>/img/g400.png" alt="Globo" />
					</div>
				</div>
				<div class="large-4 columns hblock">
					<h1 class="gabriela htext">Cierra los ojos, abre tu mente.</h1>
					<p class="htext animatetext">Dedicado al diseño y entusiasta de la fotografía, también del desarrollo de materiales visuales desde finales del 2009 con sede en Santo Domingo.</p>
				</div>
			</div>
		</div>
	</div>
</header>
<section id="section-one">
	<div class="row">
		<div class="large-10 large-centered columns">
			<h1 style="margin-bottom: 0;" class="aling-center subheader gabriela">Hecho con amor y pasión</h1>
			<p class="aling-center">creo que el combinar una buena interfaz con un correcto funcionamiento mas una gran experiencia de usuario puede hacer casi cualquier aplicación o sitio web un gran éxito.</p>
		</div>
	</div>
	<div class="row">
		<?php $my_query = new WP_Query('category_name=vector&showposts=6'); $i=0; ?>
		<?php while ($my_query->have_posts()) : $my_query->the_post(); $do_not_duplicate = $post->ID; ?>
			<div class="small-6 large-4 columns">
				<div class="blog-article">
					<a href="<?php the_permalink();?>">
					<?php the_post_thumbnail(); ?>
					</a>
					<h5 class="the-title subheader">
						<?php if (strlen($post->post_title) > 30) { 
							echo substr(the_title($before = '', $after = '', FALSE), 0, 30) . '...'; 
						} 
							else { the_title(); } ?>
					</h5>
				</div>
				<?php if ( has_post_thumbnail() ) : ?>
				<?php endif; ?>
			</div>
		<?php $i++; endwhile; ?>
	</div>
</section>
<section id="postpoint">
	<article class="row">
		<div class="large-8 columns">
			<div class="row">
				<div class="large-4 columns">
					<a class="fancybox-thumbs" data-fancybox-group="thumb" href="<?php echo get_template_directory_uri(); ?>/img/project/postpoint/postpoint-01.png">
						<img src="<?php echo get_template_directory_uri(); ?>/img/project/postpoint/tumbnail-01.png" alt="postpoint" />
					</a>
				</div>
				<div class="large-4 columns">
					<a class="fancybox-thumbs" data-fancybox-group="thumb" href="<?php echo get_template_directory_uri(); ?>/img/project/postpoint/postpoint-01.png">
						<img src="<?php echo get_template_directory_uri(); ?>/img/project/postpoint/tumbnail-01.png" alt="postpoint" />
					</a>
				</div>
				<div class="large-4 columns">
					<a class="fancybox-thumbs" data-fancybox-group="thumb" href="<?php echo get_template_directory_uri(); ?>/img/project/postpoint/postpoint-01.png">
						<img src="<?php echo get_template_directory_uri(); ?>/img/project/postpoint/tumbnail-01.png" alt="postpoint" />
					</a>
				</div>
			</div>
		</div>
		<div class="large-4 columns">4</div>
	</article>
</section>
<section id="proceso">
	<article>
		<div class="row">
			<h1 style="margin-bottom: 0;" class="aling-center subheader gabriela">Como se hace web?</h1>
			<div class="large-4 columns">
				<h4><i class="icon-search"></i> Análisis</h4>
				<p>Determinar la audiencia o publico a quien se le dirige. Es esencial, ya que muchos diseños y contenidos dependen de esta información.</p>
				<p>No solo se almacenan sus ideas sino que se complementan con sus necesidades y la mejor manera de gestionarlas para el correcto traslado de ellas a la web.</p>
			</div>
			<div class="large-4 columns">
				<h4><i class="icon-file-alt"></i> Propuestas</h4>
				<p>En base al análisis se presenta una planificación esquemática que permita la simple gestión de modificaciones en su estructura.</p>
				<p>Una propuesta debe de ser un esquema completo de que podría ser el proyecto ya concluido una vez desarrollado. Estos deben de ser discutidos y retocados según los requerimientos del cliente siempre contando con mi orientación y criterios.</p>
			</div>
			<div class="large-4 columns">
				<h4><i class="icon-wrench"></i> Desarrollo</h4>
				<p>En este paso el proyecto ya ha sido bien definido y el proceso de diseño gráfico ya está. Ahora se programa la aplicación y la base de datos para juntarlo con el diseño de la interface previamente definida.</p>
				<p>Ya bien definido el proyecto y diseño pues se pasa a aplicar técnicas y conocimientos de programación para unir funcionalidad e interfaz gráfica ya establecida.</p>
			</div>
		</div>
		<div class="row">
			<div class="large-4 columns">
				<h4><i class="icon-beaker"></i> Pruebas</h4>
				<p>Pasado el proceso del desarrollo se coloca el proyecto en un servidor y se establece la fase de pruebas.</p> 
				<p>Solo el cliente mediante usuario y contraseña puede ver el desarrollo de esta fase, el publico vera una portada o un pequeño sitio que les permitirá registrarse para recibir información sobre el lanzamiento definitivo del proyecto.</p>
			</div>
			<div class="large-4 columns">
				<h4><i class="icon-bullhorn"></i> Lanzamiento</h4>
				<p>Pues es hora de mostrar y dar de alta el proyecto web en el que tanto se estuvo trabajando.</p>
				<p>Previo a esto se le instruye sobre las mejores practicas dependiendo de la naturaleza antes descritas del mismo.</p>
				<p>Se llevara a cabo el registro en los buscadores adecuadamente y seleccionar las palabras claves y conceptos claves que definen el registro del web.</p>
			</div>
			<div class="large-4 columns">
				<h4><i class="icon-refresh"></i> Seguimiento</h4>
				<p>Todo proyecto de esta naturaleza amerita un mantenimiento constante por la constante evolución.</p>
				<p>No se debe de cometer el error de creer que estando en linea ya se hizo todo pues amerita de una observación constante para garantizar un correcto funcionamiento y que así su inversión sea la correcta.</p>
			</div>
		</div>
	</article>
</section>
<section id="followme">
	<article>
		<div class="row">
			<div class="large-2 small-6 small-centered columns large-centered">
				<img id="avatar" src="<?php echo get_template_directory_uri(); ?>/img/avatar.png" alt="Cesar Medrano" />
			</div>
		</div>
		<div class="row">
			<div class="large-10 columns large-centered">
				<h1 class="aling-center subheader gabriela">Vamos a estar en contacto</h1>
				<h4 class="aling-center subheader">¿Tienes un proyecto interesante y te gustaría que trabajemos juntos?</h4>
				<div class="row">
					<div class="large-4 columns small-6 small-centered large-centered">
						<a href="#contactos" class="button radius large-12 fancybox">Contactar</a>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="large-6 columns large-centered">
				<div class="follow-panel">
					<div class="row">
						<div class="large-3 small-3 columns">
							<a target="_blank" href="http://www.behance.net/cesarmedrano">
								<img src="<?php echo get_template_directory_uri(); ?>/img/behance-dark.png" alt="Cesar Medrano" />
							</a>
						</div>
						<div class="large-3 small-3 columns">
							<a target="_blank" href="https://www.facebook.com/pages/Cesar-Medrano/240082449394123">
								<img src="<?php echo get_template_directory_uri(); ?>/img/facebook-dark.png" alt="facebook.com/cesaremedrano" />
							</a>
						</div>
						<div class="large-3 small-3 columns">
							<a target="_blank" href="https://twitter.com/cesar_medrano">
								<img src="<?php echo get_template_directory_uri(); ?>/img/twitter-dark.png" alt="@cesar_medrano" />
							</a>
						</div>
						<div class="large-3 small-3 columns">
							<a target="_blank" href="http://www.flickr.com/photos/cesarmedrano/">
								<img src="<?php echo get_template_directory_uri(); ?>/img/flickr-dark.png" alt="Cesar Medrano" />
							</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</article>
</section>
<section id="contactos">
	<article class="row">
		<form class="contact_form" action="../mail" method="post">
		    <ul>
		        <li>
		             <h2 class="gabriela">Un correo :)</h2>
		        </li>
		        <li>
		            <label for="name">Nombre:</label>
		            <input name="nombre" type="text" required id="nombre"  placeholder="Nombre"/>
		        </li>
		        <li>
		            <label for="email">Email:</label>
		            <input type="email" name="email" placeholder="Correo electronico" required/>
		        </li>
		        <li>
		            <label for="website">Sitio Web:</label>
		            <input type="url" name="web" placeholder="sitio web"/>
		        </li>
		        <li>
		            <label for="mensaje">Mensaje:</label>
		            <textarea name="mensaje" cols="40" rows="6" required id="mensaje" ></textarea>
		        </li>
		        <li>
		        	<button class="submit button radius large-12" type="submit">Enviar</button>
		        </li>
		    </ul>
		</form>
	</article>
</section>
<?php get_sidebar( 'front' ); ?>
<?php get_footer(); ?>