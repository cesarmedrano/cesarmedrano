<?php 
/*
Template Name: mantenimiento
*/
get_header();?>
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/out.css" type="text/css" />
<section id="mantenimiento">
	<article class="row">
		<div class="large-8 columns">
			<div id="globo-content">
				<img id="globo" src="<?php echo get_template_directory_uri(); ?>/img/g400.png" alt="Globo" />
			</div>
		</div>
		<div class="large-4 columns right">
			<h1 class="gabriela">Disculpe las molestias</h1>
			<p>Por el momento estoy realizando unas labores de mantenimiento, no será por mucho tiempo</p>
			<br />
			<p class="htext animatetext">Dedicado al diseño y entusiasta de la fotografía, también del desarrollo de materiales visuales desde finales del 2009 con sede en Santo Domingo.</p>
		</div>
	</article>
	
	<article class="row">
		<div class="large-8 columns">&nbsp;</div>
		<div class="large-4 columns">
		<div class="follow-panel">
			<div class="row">
				<div class="large-3 small-3 columns">
					<a target="_blank" href="http://www.behance.net/cesarmedrano">
						<img src="<?php echo get_template_directory_uri(); ?>/img/behance-dark.png" alt="Cesar Medrano" />
					</a>
				</div>
				<div class="large-3 small-3 columns">
					<a target="_blank" href="https://www.facebook.com/pages/Cesar-Medrano/240082449394123">
						<img src="<?php echo get_template_directory_uri(); ?>/img/facebook-dark.png" alt="facebook.com/cesaremedrano" />
					</a>
				</div>
				<div class="large-3 small-3 columns">
					<a target="_blank" href="https://twitter.com/cesar_medrano">
						<img src="<?php echo get_template_directory_uri(); ?>/img/twitter-dark.png" alt="@cesar_medrano" />
					</a>
				</div>
				<div class="large-3 small-3 columns">
					<a target="_blank" href="http://www.flickr.com/photos/cesarmedrano/">
						<img src="<?php echo get_template_directory_uri(); ?>/img/flickr-dark.png" alt="Cesar Medrano" />
					</a>
				</div>
			</div>
		<a href="/">&copy; <?php the_date('Y'); ?> <?php bloginfo('name'); ?></a>
		
		</div>
	</article>
</section>