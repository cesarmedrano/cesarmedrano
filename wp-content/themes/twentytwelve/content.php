<?php
/**
 * The default template for displaying content. Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */
?>

<article class="post" id="post-<?php the_ID(); ?>">
	<?php if ( is_sticky() && is_home() && ! is_paged() ) : ?>
	<!--<div class="featured-post">
		<?php _e( 'Featured post', 'twentytwelve' ); ?>
	</div>-->
	<?php endif; ?>
	<header class="entry-header row">
		<div class="large-6 columns">
			<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
		</div>
		<div class="large-6 columns">
			<?php if ( is_single() ) : ?>
			<h3 class="gabriela subheader"><?php the_title(); ?></h3>
			<time datetime="<?php the_time('Y-m-d') ?>"><?php the_time('l j F, Y') ?></time>
			<?php else : ?>
			<h3 class="gabriela subheader">
				<a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( sprintf( __( 'Permalink to %s', 'twentytwelve' ), the_title_attribute( 'echo=0' ) ) ); ?>" rel="bookmark"><?php the_title(); ?></a>
			</h3>
			
			<time datetime="<?php the_time('Y-m-d') ?>"><?php the_time('l j F, Y') ?></time>
			<?php endif; // is_single() ?>
			<?php if ( comments_open() ) : ?>
				<div class="comments-link">
				</div><!-- .comments-link -->
			<?php endif; // comments_open() ?>
			<?php if ( is_search() ) : // Only display Excerpts for Search ?>
			<div class="entry-summary">
				<?php the_excerpt(); ?>
			</div><!-- .entry-summary -->
			<?php else : ?>
			<div class="entry-content">
			
				<?php if (strlen($post->post_texto) > 20) { 
					echo substr(the_content($before = '', $after = '', FALSE), 0, 30) . '...'; 
				} 
					else { the_content( __( '') ); } ?>
				<?php wp_link_pages( array( 'before' => '<div class="page-links">' . __( 'Pages:'), 'after' => '</div>' ) ); ?>
			</div><!-- .entry-content -->
			<?php endif; ?>
		</div>
	</header><!-- .entry-header -->
</article>

