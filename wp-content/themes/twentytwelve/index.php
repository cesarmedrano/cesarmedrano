<?php
/**
 * @package WordPress
 * @subpackage Base
 * @since base 1.0
 */

get_header(); ?>
<header id="h-blog-page" role="banner">
	<div class="row">
		<div class="large-1 small-4 columns">
			<a href="/" class="hint  hint--right" data-hint="César Medrano">
				<img src="<?php echo get_template_directory_uri(); ?>/img/logo-footer.png" alt="logo" />
			</a>
		</div>
		<div class="large-11 small-8 columns">
			<?php
			
			$defaults = array(
				'theme_location'  => '',
				'menu'            => '',
				'container'       => 'nav',
				'container_class' => '',
				'container_id'    => '',
				'menu_class'      => 'sub-nav',
				'menu_id'         => '',
				'echo'            => true,
				'fallback_cb'     => 'wp_page_menu',
				'before'          => '',
				'after'           => '',
				'link_before'     => '',
				'link_after'      => '',
				'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
				'depth'           => 0,
				'walker'          => ''
			);
			
			wp_nav_menu( $defaults );
			
			?>
		</div>
	</div>
</header>
<div class="row">
	<section class="large-8 columns">
		<?php if ( have_posts() ) : ?>
		
					<?php /* Start the Loop */ ?>
					<?php while ( have_posts() ) : the_post(); ?>
						<?php get_template_part( 'content', get_post_format() ); ?>
					<?php endwhile; ?>
		
					<?php twentytwelve_content_nav( 'nav-below' ); ?>
		
				<?php else : ?>
		
					<article id="post-0" class="post no-results not-found">
		
					<?php if ( current_user_can( 'edit_posts' ) ) :
						// Show a different message to a logged-in user who can add posts.
					?>
						<header class="entry-header">
							<h1 class="entry-title gabriela"><?php _e( 'No posts to display', 'twentytwelve' ); ?></h1>
						</header>
		
						<div class="entry-content">
							<p><?php printf( __( 'Ready to publish your first post? <a href="%s">Get started here</a>.', 'twentytwelve' ), admin_url( 'post-new.php' ) ); ?></p>
						</div><!-- .entry-content -->
		
					<?php else :
						// Show the default message to everyone else.
					?>
						<header class="entry-header">
							<h1 class="entry-title gabriela"><?php _e( 'Nothing Found', 'twentytwelve' ); ?></h1>
						</header>
		
						<div class="entry-content">
							<p><?php _e( 'Apologies, but no results were found. Perhaps searching will help find a related post.', 'twentytwelve' ); ?></p>
							<?php get_search_form(); ?>
						</div><!-- .entry-content -->
					<?php endif; // end current_user_can() check ?>
		
					</article><!-- #post-0 -->
		
		<?php endif; // end have_posts() check ?>
	</section>
	<section class="large-4 columns"><!--<?php get_sidebar(); ?>-->
<!--SEARCH-->
		<article id="form-search">
			<form class="searchform" method="get" action="<?php echo $_SERVER['PHP_SELF']; ?>">
			<div class="row">
				<div class="large-7 small-8 columns"><input type="text" name="s" id="s" class="large-12"/></div>
				<div class="large-5 small-4 columns">
					<input class="button large-12" type="submit" value="<?php _e('Search'); ?>" />
				</div>
			</div>
			</form>
		</article>
<!--SEARCH-->
<!--RECENT POST-->
		<article class="widget-content">
			<h4 class="widget-tittle">Recientes</h4>
			<ul class="mywidget">
			<?php
				$args = array( 'numberposts' => '5', 'tax_query' => array(
						array(
							'taxonomy' => 'post_format',
							'field' => 'slug',
							'terms' => 'post-format-aside',
							'operator' => 'NOT IN'
						), 
						array(
							'taxonomy' => 'post_format',
							'field' => 'slug',
							'terms' => 'post-format-image',
			 				'operator' => 'NOT IN'
						)
				) );
				$recent_posts = wp_get_recent_posts( $args );
				foreach( $recent_posts as $recent ){
					echo '<li><a href="' . get_permalink($recent["ID"]) . '" title="Look '.esc_attr($recent["post_title"]).'" >' .   $recent["post_title"].'</a> </li> ';
				}
			?>
			</ul>
		</article>
<!--RECENT POST-->

<!--FOLLOW-->
		<div class="follow">
			<div class="row">
				<div class="large-3 small-3 columns">
					<a target="_blank" href="http://www.behance.net/cesarmedrano">
						<img src="<?php echo get_template_directory_uri(); ?>/img/behance.png" alt="Cesar Medrano" />
					</a>
				</div>
				<div class="large-3 small-3 columns">
					<a target="_blank" href="https://www.facebook.com/pages/Cesar-Medrano/240082449394123">
						<img src="<?php echo get_template_directory_uri(); ?>/img/facebook.png" alt="facebook.com/cesaremedrano" />
					</a>
				</div>
				<div class="large-3 small-3 columns">
					<a target="_blank" href="https://twitter.com/cesar_medrano">
						<img src="<?php echo get_template_directory_uri(); ?>/img/twitter.png" alt="@cesar_medrano" />
					</a>
				</div>
				<div class="large-3 small-3 columns">
					<a target="_blank" href="http://www.flickr.com/photos/cesarmedrano/">
						<img src="<?php echo get_template_directory_uri(); ?>/img/flickr.png" alt="Cesar Medrano" />
					</a>
				</div>
			</div>
		</div>
<!--FOLLOW-->		
	</section> 
</div>

<?php get_footer(); ?>