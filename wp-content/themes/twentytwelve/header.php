<?php
/**
 * @package WordPress
 * @subpackage Base
 * @since base 1.0
 */
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8)  ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width" />
<title><?php wp_title( '|', true, 'right' ); ?></title>
<link href="<?php echo get_template_directory_uri(); ?>/img/favicon.ico" rel="icon" type="image/x-icon" />
<link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/img/favicon.png" />
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/normalize.css" />
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/foundation.css" />
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/general_foundicons.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/hint.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/jquery.fancybox.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/font-awesome.css" />
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/app.css" />
<?php // Loads HTML5 JavaScript file to add support for HTML5 elements in older IE versions. ?>
<!--[if lt IE 9]>
<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js" type="text/javascript"></script>
<![endif]-->
<!--[if lt IE 8]>
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/general_foundicons_ie7.css">
<![endif]-->
<!--JS-->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js" type="text/javascript"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/jquery.fancybox.pack.js" type="text/javascript"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/tweet.js" type="text/javascript"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/foundation.min.js" type="text/javascript"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/vendor/custom.modernizr.js" type="text/javascript"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/html5.js" type="text/javascript"></script>
<script src="<?php echo get_template_directory_uri(); ?>/js/app.js" type="text/javascript"></script>
<script>
document.write('<script src=' +
('__proto__' in {} ? '<?php echo get_template_directory_uri(); ?>/js/vendor/zepto' : 'js/vendor/jquery') +
'.js><\/script>')
</script>
<script>
  $(document).foundation();
</script>
<!--Analitys-->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-38727329-1']);
  _gaq.push(['_setDomainName', 'cesarmedrano.com']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
<!--Analitys-->
<!--JS-->
<?php wp_head(); ?>
</head>