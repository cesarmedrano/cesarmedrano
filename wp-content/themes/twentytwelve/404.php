<?php
/**
 * The template for displaying 404 pages (Not Found).
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */

get_header(); ?>
<section id="error404">
<article class="row">
	<div class="large-6 columns">
		<img src="<?php echo get_template_directory_uri(); ?>/img/post-0.png" alt="Moom" />
	</div>
	<div class="large-6 columns">
		<h1 class="gabriela"><?php _e( 'Error 404: Archivo no encontrado'); ?></h1>
		<p><?php _e( 'It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help.', 'twentytwelve' ); ?></p>
		<div id="form-search">
			<form id="searchform" method="get" action="<?php echo $_SERVER['PHP_SELF']; ?>">
			<div class="row">
				<div class="large-8 small-8 mediun-8 columns"><input type="text" name="s" id="s" class="large-12"/></div>
				<div class="large-4 small-4 mediun-8 columns">
					<input class="button large-12" type="submit" value="<?php _e('Search'); ?>" />
				</div>
			</div>
			</form>
		</div>
	</div>
</article>
<section>
	<footer>
		<div class="row">
			<div class="large-12 columns">
				<?php
				
				$defaults = array(
					'theme_location'  => '',
					'menu'            => '',
					'container'       => 'div',
					'container_class' => '',
					'container_id'    => '',
					'menu_class'      => 'sub-nav',
					'menu_id'         => '',
					'echo'            => true,
					'fallback_cb'     => 'wp_page_menu',
					'before'          => '',
					'after'           => '',
					'link_before'     => '',
					'link_after'      => '',
					'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
					'depth'           => 0,
					'walker'          => ''
				);
				
				wp_nav_menu( $defaults );
				
				?>
			</div>
		</div>
	</footer>
</section>
</section>
