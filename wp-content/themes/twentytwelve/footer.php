<?php
/**
 * The template for displaying the footer.
 *
 * Contains footer content and the closing of the
 * #main and #page div elements.
 *
 * @package WordPress
 * @subpackage Twenty_Twelve
 * @since Twenty Twelve 1.0
 */
?>
	<section id="footer">
		<footer>
			<div class="row">
				<div class="large-4 small-4 columns">
					<a href="/">&copy; <?php the_date('Y'); ?> <?php bloginfo('name'); ?></a>
				</div>
				<div class="large-8 small-8 columns">
					<?php
					
					$defaults = array(
						'theme_location'  => '',
						'menu'            => '',
						'container'       => 'div',
						'container_class' => 'right',
						'container_id'    => '',
						'menu_class'      => 'sub-nav',
						'menu_id'         => '',
						'echo'            => true,
						'fallback_cb'     => 'wp_page_menu',
						'before'          => '',
						'after'           => '',
						'link_before'     => '',
						'link_after'      => '',
						'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
						'depth'           => 0,
						'walker'          => ''
					);
					
					wp_nav_menu( $defaults );
					
					?>
				</div>
			</div>
			<div class="row">
				<div class="small-4 large-2 columns large-centered small-centered">
					<a href="http://validator.w3.org/check?uri=http%3A%2F%2Fcesarmedrano.com%2F" target="_blank">
					<img src="http://www.w3.org/html/logo/badge/html5-badge-h-css3.png" width="133" height="64" alt="HTML5 Powered with CSS3 / Styling" title="HTML5 Powered with CSS3 / Styling">
					</a>
				</div>
			</div>
		</footer>
	</section>
	</body>
</html>