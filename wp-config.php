<?php
/** 
 * Configuración básica de WordPress.
 *
 * Este archivo contiene las siguientes configuraciones: ajustes de MySQL, prefijo de tablas,
 * claves secretas, idioma de WordPress y ABSPATH. Para obtener más información,
 * visita la página del Codex{@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} . Los ajustes de MySQL te los proporcionará tu proveedor de alojamiento web.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** Ajustes de MySQL. Solicita estos datos a tu proveedor de alojamiento web. ** //
/** El nombre de tu base de datos de WordPress */
define('DB_NAME', 'cesarmeddb');

/** Tu nombre de usuario de MySQL */
define('DB_USER', 'root');

/** Tu contraseña de MySQL */
define('DB_PASSWORD', 'root');

/** Host de MySQL (es muy probable que no necesites cambiarlo) */
define('DB_HOST', 'localhost:8888');

/** Codificación de caracteres para la base de datos. */
define('DB_CHARSET', 'utf8');

/** Cotejamiento de la base de datos. No lo modifiques si tienes dudas. */
define('DB_COLLATE', '');

/**#@+
 * Claves únicas de autentificación.
 *
 * Define cada clave secreta con una frase aleatoria distinta.
 * Puedes generarlas usando el {@link https://api.wordpress.org/secret-key/1.1/salt/ servicio de claves secretas de WordPress}
 * Puedes cambiar las claves en cualquier momento para invalidar todas las cookies existentes. Esto forzará a todos los usuarios a volver a hacer login.
 *
 * @since 2.6.0
 */
define('AUTH_KEY', '3g!]7)>bFq,`>:8DyPZY|]iTmvKhP ZFCx}V^uocU!bsO:-cJF6d$b<prckQi1-x'); // Cambia esto por tu frase aleatoria.
define('SECURE_AUTH_KEY', '^QF=My=Q?!?)2Xw,2P15!}#|v8]^]}%fw)s-z|v7FtsnNY5ocD|6 ZH%$ORZ])jD'); // Cambia esto por tu frase aleatoria.
define('LOGGED_IN_KEY', 'hc?@)bROeZ0;OahW^-xB W0[m~6%`|<^&+`lTs8vb84(-|7S$Xg>MUv0`3/6,LYK'); // Cambia esto por tu frase aleatoria.
define('NONCE_KEY', ' 8@ !,]}NBMBr:kVOx0ED%PRMp|KSj%KlfnJppaq<s^j23NN,vErL+i(LTFb?jX8'); // Cambia esto por tu frase aleatoria.
define('AUTH_SALT', 'X,()wy~|yNzqU-|MB`1(,^sd>gv=U;lmrWH<A+0Aw^,goW&?|sAVoaA~+W!(,Irg'); // Cambia esto por tu frase aleatoria.
define('SECURE_AUTH_SALT', 'Kg=~hQ@L]U`HUE%nLE/,APzuRB@zB([>zY)bYPe8ZGb=w9tt$0(Gy`u%J| .*;Q3'); // Cambia esto por tu frase aleatoria.
define('LOGGED_IN_SALT', '1U{?r@q 6iRmfdfb3},HbGtA|?ZBRs+OJo*:.!AF4UU{VB5TZ{3E;klqDoX1!W)F'); // Cambia esto por tu frase aleatoria.
define('NONCE_SALT', '/-U4!xD,U/AG0iEc;c!%5W<k-io1{^l|$ CY:IsiW%iw2xFgR29V~`YExht[{FkO'); // Cambia esto por tu frase aleatoria.

/**#@-*/

/**
 * Prefijo de la base de datos de WordPress.
 *
 * Cambia el prefijo si deseas instalar multiples blogs en una sola base de datos.
 * Emplea solo números, letras y guión bajo.
 */
$table_prefix  = 'wp_';

/**
 * Idioma de WordPress.
 *
 * Cambia lo siguiente para tener WordPress en tu idioma. El correspondiente archivo MO
 * del lenguaje elegido debe encontrarse en wp-content/languages.
 * Por ejemplo, instala ca_ES.mo copiándolo a wp-content/languages y define WPLANG como 'ca_ES'
 * para traducir WordPress al catalán.
 */
define('WPLANG', 'es_ES');

/**
 * Para desarrolladores: modo debug de WordPress.
 *
 * Cambia esto a true para activar la muestra de avisos durante el desarrollo.
 * Se recomienda encarecidamente a los desarrolladores de temas y plugins que usen WP_DEBUG
 * en sus entornos de desarrollo.
 */
define('WP_DEBUG', false);

/* ¡Eso es todo, deja de editar! Feliz blogging */

/** WordPress absolute path to the Wordpress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

